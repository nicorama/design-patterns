package io.robusta.patterns.composite;

public interface HasPriceComposite extends HasPriceComponent {

	public void addComponent(HasPriceComponent component);
	
}
