package io.robusta.patterns.composite;

public interface HasPriceComponent {
	
	public int getPrice();
	
}
