package io.robusta.patterns.domain;


import io.robusta.patterns.abstractfactory.MeganCoupeFactory;
import io.robusta.patterns.abstractfactory.PuntoFactory;

public class CarStore {

	MeganCoupeFactory meganFactory = new MeganCoupeFactory();
	PuntoFactory puntoFactory = new PuntoFactory();
	
	public Car buyCar(String futureOwner, boolean megan){
		
		System.out.println("Dealer : Hey mister");
		System.out.println("Customer : I'd like a car ; Here is 40000$");
		Car car;
		if (megan){
			car = meganFactory.getCar();
		}else{
			car = puntoFactory.getCar();
		}
		System.out.println("Dealer : Here it is, it's even cheaper than 40k$");
		car.setOwner(futureOwner);
		System.out.println("Customer : Cool, nice car... But is it exactly what I've asked ?");
		System.out.println("Dealer : Sure. See you :)");
		car.start();
		return car;
	}
	
}
