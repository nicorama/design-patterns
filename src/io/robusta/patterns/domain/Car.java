package io.robusta.patterns.domain;

public abstract class Car {

	int wheels = 4;
	boolean motorized = true;
	String owner = null;
	
	
	public int getWheels() {
		return wheels;
	}
	public void setWheels(int wheels) {
		this.wheels = wheels;
	}
	public boolean isMotorized() {
		return motorized;
	}
	public void setMotorized(boolean motorized) {
		this.motorized = motorized;
	}
	
	
	public abstract Constructor getConstructor();
	
	public abstract String getName();
	
	public abstract int getPrice();
	
	public abstract boolean hasBonus();
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	
	public void start(){
		System.out.println(this.getName()+ " says Vroooom");
	}
	
	
	
}
