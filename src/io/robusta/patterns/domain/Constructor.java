package io.robusta.patterns.domain;

public class Constructor {

	
	String name;
	String country;
	
	public Constructor(String name, String country) {

		this.name = name;
		this.country = country;
	}
	
	
	public String getName() {
		return name;
	}

	//name won't change

	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}
	
	
	
	
}
