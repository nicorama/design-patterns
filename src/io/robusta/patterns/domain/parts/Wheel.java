package io.robusta.patterns.domain.parts;

import io.robusta.patterns.composite.HasPriceComponent;

public class Wheel implements HasPriceComponent  {

	
	boolean sport = false;
	
	public Wheel() {
	}
	
	public Wheel(boolean sport) {
		this.sport = sport;
	}
	
	@Override
	public int getPrice() {
		return sport ? 3000 : 300;
	}	
	
}
