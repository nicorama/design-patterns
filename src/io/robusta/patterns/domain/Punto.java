package io.robusta.patterns.domain;

public class Punto extends CityCar{

	
	
	

	@Override
	public Constructor getConstructor() {
		return new Constructor("Volkswagen", "Deutschland");
	}

	@Override
	public String getName() {
		return "Punto";
	}

	@Override
	public int getPrice() {
		return 25000;
	}

}
