package io.robusta.patterns.domain;

public class MeganeCoupeRS  extends SportCar{


	@Override
	public String[] getPackage() {
		return new String[]{"6 cylindre en V", "Baquet sport", "Triple turbine"};
	}

	@Override
	public Constructor getConstructor() {
		//Ouch !!
		return new Constructor("Renault", "France");
	}

	@Override
	public String getName() {		
		return "Mégane Coupé RS";
	}

	@Override
	public int getPrice() {
		// TODO Auto-generated method stub
		return 30000;
	}

}
