package io.robusta.patterns.abstractfactory;

import io.robusta.patterns.domain.Car;
import io.robusta.patterns.domain.MeganeCoupeRS;

public class MeganCoupeFactory extends AbstractCarFactory{

	@Override
	public Car getCar() {
		return new MeganeCoupeRS();
	}

}
