package io.robusta.patterns.abstractfactory;

import io.robusta.patterns.domain.Car;
import io.robusta.patterns.domain.Punto;

public class PuntoFactory extends AbstractCarFactory{

	@Override
	public Car getCar() {
		return new Punto();
	}

	
}
