package io.robusta.patterns.abstractfactory;

import io.robusta.patterns.domain.Car;
import io.robusta.patterns.domain.CarStore;

public abstract class AbstractCarFactory {

	
	public abstract Car getCar();

	
	
	public static void main(String[] args) {
		CarStore store = new CarStore();
		store.buyCar("John Doe", true);
	}
}
