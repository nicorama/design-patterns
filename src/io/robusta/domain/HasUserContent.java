package io.robusta.domain;

public interface HasUserContent {

	
	public String getUserContent();
}
