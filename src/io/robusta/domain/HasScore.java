package io.robusta.domain;

public interface HasScore {

	public int getScore();
}
