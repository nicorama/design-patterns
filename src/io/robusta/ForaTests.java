package io.robusta;

import io.robusta.business.TopicBusiness;

public class ForaTests {

	
	public static void main(String[] args) {
		ForaDataSource fora = new ForaDataSource();
		fora.initDataSource();
		
		TopicBusiness topicBusiness = new TopicBusiness();
		
		System.out.println("Il y a : " + topicBusiness.countTopics() + " topics");
	}
}
